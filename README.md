# Lirio

Lirio is a Svelte package designed for use with the [Netlify Visual Editor](https://docs.netlify.com/visual-editor/overview/), or as a standalone component library.
With just a little configuration and a few minutes of work, you can have a beautiful site builder ready to start making content.