export * from "./headers";
export * from "./teams";
export * from "./meta";
export * from "./content";
export * from "./menus";
export * from "./footers";
export * from "./ctas";
export * from "./embeds";
export * from "./modals";

import { HeaderBlocks } from './headers';
import { MetaBlocks } from "./meta";
import { TeamBlocks } from "./teams";
import { ContentBlocks } from "./content";
import { CTABlocks } from "./ctas";
import { EmbedBlocks } from "./embeds";

export const LirioBlocks = [
    ...HeaderBlocks,
    ...MetaBlocks,
    ...TeamBlocks,
    ...ContentBlocks,
    ...CTABlocks,
    ...EmbedBlocks
]