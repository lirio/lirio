import type { Model } from "@stackbit/types";

import { model as TeamWithImageAndParagraphModel } from "./TeamWithImageAndParagraph.js";

const models: Model[] = [
    TeamWithImageAndParagraphModel
];

export default models;