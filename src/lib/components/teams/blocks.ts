import TeamWithImageAndParagraph from "./TeamWithImageAndParagraph.svelte";

import { blockReferenceID as TeamWithImageAndParagraphID } from "./TeamWithImageAndParagraph.js";

const blocks = [
    { blockReferenceID: TeamWithImageAndParagraphID, component: TeamWithImageAndParagraph },
];

export default blocks;