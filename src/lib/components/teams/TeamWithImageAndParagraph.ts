import type { Model } from "@stackbit/types";

export const blockReferenceID = "team-with-image-and-paragraph";
export const model = {
    name: "TeamWithImageAndParagraph",
    type: "object",
    groups: ["LirioBlock"],
    fields: [
        {
            name: "blockReferenceID",
            type: "string",
            const: "team-with-image-and-paragraph",
            hidden: true
        },
        {
            name: "Heading",
            type: "string",
            default: "Heading",
            required: true
        },
        {
            name: "Description",
            type: "markdown",
            default: "Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui lorem cupidatat commodo. Elit sunt amet fugiat veniam occaecat fugiat aliqua.",
            required: true
        },
        {
            name: "TeamCards",
            type: "list",
            required: true,
            items: {
                type: 'model',
                models: ['TeamCard']
            }
            
        }
    ]
} satisfies Model;
