import type { Model } from "@stackbit/types";

import { model as MetaModel } from "./Meta.js";

const models: Model[] = [
    MetaModel
];

export default models;