import type { Model } from "@stackbit/types";

export const blockReferenceID = "meta";
export const model: Model = {
    name: "Meta",
    type: "object",
    groups: ["LirioBlock"],
    fields: [
        {
            name: "Title",
            type: "string"
        }
    ]
};