import Meta from "./Meta.svelte";

import { blockReferenceID as MetaID } from "./Meta";

const blocks = [
    {blockReferenceID: MetaID, component: Meta}
];

export default blocks;