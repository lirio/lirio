import CTAWithImage from "./CTAWithImage.svelte";

import { blockReferenceID as CTAWithImageID } from "./CTAWithImage.js";

const blocks = [
    { blockReferenceID: CTAWithImageID, component: CTAWithImage },
];

export default blocks;