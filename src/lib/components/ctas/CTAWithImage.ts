import type { Model } from "@stackbit/types";

export const blockReferenceID = "cta-with-image";
export const model = {
    name: "CTAWithImage",
    type: "object",
    groups: ["LirioBlock"],
    fields: [
        {
            name: "blockReferenceID",
            type: "string",
            const: "cta-with-image",
            hidden: true
        },
        {
            name: "Heading",
            type: "string",
            default: "Heading",
            required: true
        },
        {
            name: "Body",
            type: "markdown",
            default: "Lorem ipsum odor amet, consectetuer adipiscing elit. Nascetur quam arcu varius urna dui magnis. \n\n- Bullet",
        },
        {
            name: "Buttons",
            type: "list",
            items: {
                type: "model",
                models: ["Button"]
            }, 
            default: []
        },
        {
            name: "Image",
            type: "model",
            models: ["Image"]
        }
    ]
} satisfies Model;
