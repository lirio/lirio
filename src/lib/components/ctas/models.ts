import type { Model } from "@stackbit/types";

import { model as CTAWithImageModel } from "./CTAWithImage.js";

const models: Model[] = [
    CTAWithImageModel
];

export default models;