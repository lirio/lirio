import HeaderSimpleWithEyebrow from "./HeaderSimpleWithEyebrow.svelte";
import HeaderSimpleWithEyebrowBackground from "./HeaderSimpleWithEyebrowBackground.svelte";

import { blockReferenceID as HeaderSimpleWithEyebrowID } from "./HeaderSimpleWithEyebrow";
import { blockReferenceID as HeaderSimpleWithEyebrowBackgroundID } from "./HeaderSimpleWithEyebrowBackground";

const blocks = [
    { blockReferenceID: HeaderSimpleWithEyebrowID, component: HeaderSimpleWithEyebrow },
    { blockReferenceID: HeaderSimpleWithEyebrowBackgroundID, component: HeaderSimpleWithEyebrowBackground },
];

export default blocks;