import type { Model } from "@stackbit/types";

export const blockReferenceID = "header-simple-with-eyebrow-background";
export const model = {
    name: "HeaderSimpleWithEyebrowBackground",
    type: 'object',
    groups: ["LirioBlock"],
    fields: [
        {
            name: "Heading",
            type: "string",
            default: "Heading",
            required: true
        },
        {
            name: "EyebrowText",
            type: "string",
            default: "Eyebrow Text",
            required: true
        },
        {
            name: "Body",
            type: "string",
            default: "Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui lorem cupidatat commodo. Elit sunt amet fugiat veniam occaecat fugiat aliqua.",
            required: true
        },
        {
            name: "blockReferenceID",
            type: "string",
            const: "header-simple-with-eyebrow-background",
            hidden: true
        },
        {
            name: 'Styles',
            type: 'style',
            styles: {
                self: { 
                    width: '*',
                    textAlign: ['left', 'center', 'right']
                }
            },
            default: {
                self: { 
                    width: "wide",
                    textAlign: "left"
                }
            }
        },
        {
            name: "BackgroundImage",
            type: "model",
            models: ['BackgroundImage']
        }
    ]
} satisfies Model;
