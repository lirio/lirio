import type { Model } from "@stackbit/types";

import { model as HeaderSimpleWithEyebrowModel } from "./HeaderSimpleWithEyebrow.js";
import { model as HeaderSimpleWithEyebrowBackgroundModel } from "./HeaderSimpleWithEyebrowBackground.js";

const models: Model[] = [
    HeaderSimpleWithEyebrowModel,
    HeaderSimpleWithEyebrowBackgroundModel,
];

export default models;