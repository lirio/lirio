import type { Model } from "@stackbit/types";

export const blockReferenceID = "content-with-sticky-image";
export const model = {
    name: "ContentWithStickyImage",
    type: "object",
    groups: ["LirioBlock"],
    fields: [
        {
            name: "blockReferenceID",
            type: "string",
            const: "content-with-sticky-image",
            hidden: true
        },
        {
            name: "Heading",
            type: "string",
            default: "Heading",
            required: true
        },
        {
            name: "EyebrowText",
            type: "string",
            default: "Eyebrow Text",
            required: false
        },
        {
            name: "Body",
            type: "markdown",
            default: "Lorem ipsum odor amet, consectetuer adipiscing elit. Nascetur quam arcu varius urna dui magnis. Venenatis phasellus cubilia turpis justo ultricies hendrerit, at molestie. Est rhoncus at metus pretium, inceptos vestibulum euismod. Venenatis justo pulvinar; tristique nec porttitor justo integer. Semper fusce viverra ridiculus etiam habitasse lacinia aenean. Ullamcorper donec sed, dolor purus natoque etiam.\n\nUltrices viverra odio nascetur placerat quis; vivamus tortor sit proin. Efficitur nunc etiam pretium mattis metus dictumst purus praesent. Auctor proin purus dolor fusce; eget pretium tincidunt. Dapibus penatibus enim vehicula potenti in adipiscing enim nunc velit. Mauris suscipit lacinia quis aenean eget sagittis primis tristique. Massa porttitor sapien inceptos ac laoreet praesent. Ligula blandit enim velit; congue aliquet cursus vulputate tempus ipsum.\n\nPenatibus duis quam dui libero ultrices nam. Ad hac nisl; platea adipiscing molestie porta leo. Egestas libero urna bibendum nisi nam netus cubilia? Fusce montes consectetur quam massa tincidunt rhoncus lacus proin. Rhoncus aliquet ultricies ridiculus sociosqu primis litora velit viverra sed. Fringilla ac nisl mus nunc tortor vestibulum mauris non commodo. Taciti amet fermentum mauris sociosqu in feugiat orci.\n\nSuscipit semper maecenas phasellus maecenas mattis mollis ex molestie in. Natoque phasellus fermentum hendrerit dis luctus urna; dictum eu mi. Class neque non dis porta libero turpis varius. Habitasse sodales tincidunt, interdum tellus erat curae senectus. Mi convallis sociosqu ut id etiam nisi. Amet consequat cursus nam etiam phasellus ornare. Curabitur vehicula porta est; ultricies eros litora.\n\nQuis neque nulla torquent nisl, tempus lectus lacinia purus. Dictum orci eget eget imperdiet parturient hendrerit eu. Nostra congue netus est mi elementum. Etiam neque aliquam conubia est vitae condimentum nec dictum primis. Fermentum gravida nisi senectus aptent integer semper tortor. Duis aenean fringilla cursus iaculis posuere. Elementum congue orci bibendum nibh at nibh; a conubia habitasse. Tincidunt ornare bibendum tincidunt neque iaculis duis dolor.",
            required: true
        },
        {
            name: "ImageLocation",
            type: "enum",
            controlType: 'button-group',
            default: 'left',
            options: [
                { label: "Left", value: "left" },
                { label: "Right", value: "right" }
            ]
        },
        {
            name: "Image",
            type: "model",
            models: ["Image"]
        }
    ]
} satisfies Model;
