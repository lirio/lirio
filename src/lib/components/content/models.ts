import type { Model } from "@stackbit/types";

import { model as ContentWithStickImageModel } from "./ContentWithStickyImage.js";

const models: Model[] = [
    ContentWithStickImageModel
];

export default models;