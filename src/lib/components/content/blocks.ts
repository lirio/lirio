import ContentWithStickyImage from "./ContentWithStickyImage.svelte";

import { blockReferenceID as ContentWithStickyImageID } from "./ContentWithStickyImage.js";

const blocks = [
    { blockReferenceID: ContentWithStickyImageID, component: ContentWithStickyImage },
];

export default blocks;