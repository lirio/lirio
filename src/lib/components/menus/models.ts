import type { Model } from "@stackbit/types";

import { model as MenuSettingsModel } from "./MenuSettings.js";

const models: Model[] = [
    MenuSettingsModel
];

export default models;