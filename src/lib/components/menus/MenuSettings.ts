import type { Model } from "@stackbit/types";

export const blockReferenceID = "menu";
export const model = {
    name: "MenuSettings",
    description: "Settings for the top Menu",
    type: "object",
    fields: [
        {
            name: "Icon",
            type: "model",
            models: ['Icon'],
            default: "/favicon.png",
            required: true
        },
        {
            name: "Links",
            type: "list",
            items: {
                type: "model",
                models: ["Link"]
            },
            default: []
        },
        {
            name: "Menus",
            type: "list",
            items: {
                type: "model",
                models: ["PopupMenu"]
            },
            default: []
        },
        {
            name: "ShowShareButton",
            type: "boolean",
            default: true
        }
    ]
} satisfies Model;
