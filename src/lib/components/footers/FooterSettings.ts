import type { Model } from "@stackbit/types";

export const model = {
    name: "FooterSettings",
    description: "Settings for the bottom Footer",
    type: "object",
    fields: [
        {
            name: "Icon",
            type: "model",
            models: ['Icon'],
            default: "/favicon.png",
            required: true
        },
        {
            name: "MissionStatement",
            type: "string",
            default: "Mission Statement",
            required: false
        },
        {
            name: "Categories",
            type: "list",
            items: {
                type: "object",
                fields: [
                    {
                        name: "Label",
                        type: "string",
                        default: "Category",
                        required: true
                    },
                    {
                        name: "Links",
                        type: "list",
                        items: {
                            type: "model",
                            models: ["Link"]
                        },
                        default: []
                    }

                ]
            },
            default: []
        }
    ]
} satisfies Model;
