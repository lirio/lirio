import type { Model } from "@stackbit/types";

import { model as FooterSettingsModel } from "./FooterSettings.js";

const models: Model[] = [
    FooterSettingsModel
];

export default models;