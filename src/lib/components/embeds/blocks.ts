import GiveSendGo from "./GiveSendGo.svelte";

import { blockReferenceID as GiveSendGoID } from "./GiveSendGo.js";

const blocks = [
    { blockReferenceID: GiveSendGoID, component: GiveSendGo },
];

export default blocks;