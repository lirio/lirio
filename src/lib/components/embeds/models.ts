import type { Model } from "@stackbit/types";

import { model as GiveSendGoModel } from "./GiveSendGo.js";

const models: Model[] = [
    GiveSendGoModel
];

export default models;