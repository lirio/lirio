import type { Model } from "@stackbit/types";

export const blockReferenceID = "givesendgo";
export const model = {
    name: "GiveSendGoWidget",
    type: "object",
    groups: ["LirioBlock"],
    fields: [
        {
            name: "blockReferenceID",
            type: "string",
            const: "givesendgo",
            hidden: true
        },
        {
            name: "CampaignID",
            type: "string",
            required: true
        }
    ]
} satisfies Model;
