import type { Model } from "@stackbit/types";

import { default as HeaderModels } from "$lib/components/headers/models.js";
import { default as MetaModels } from "$lib/components/meta/models.js";
import { default as TeamSectionModels } from "$lib/components/teams/models.js";
import { default as ContentModels } from "$lib/components/content/models.js";
import { default as MenuModels } from "$lib/components/menus/models.js";
import { default as FooterModels } from "$lib/components/footers/models.js";
import { default as CTAModels } from "$lib/components/ctas/models.js"; 
import { default as EmbedModels } from "$lib/components/embeds/models.js";

export const LirioModels: Model[] = [
    ...HeaderModels,
    ...TeamSectionModels,
    ...MetaModels,
    ...ContentModels,
    ...MenuModels,
    ...FooterModels,
    ...CTAModels,
    ...EmbedModels
]