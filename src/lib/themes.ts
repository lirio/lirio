import type { CustomThemeConfig } from '@skeletonlabs/tw-plugin';

export const themes = [
    {
        "display": "👑 Royal Slate",
        "value": "royal-slate"
    },
    {
        "display": "💀 Skeleton",
        "value": "skeleton"
    },
    {
        "display": "🌨️ Wintry",
        "value": "wintry"
    },
    {
        "display": "🤖 Modern",
        "value": "modern"
    },
    {
        "display": "🚀 Rocket",
        "value": "rocket"
    },
    {
        "display": "🧜‍♀️ Seafoam",
        "value": "seafoam"
    },
    {
        "display": "📺 Vintage",
        "value": "vintage"
    },
    {
        "display": "🏜️ Sahara",
        "value": "sahara"
    },
    {
        "display": "👔 Hamlindigo",
        "value": "hamlindigo"
    },
    {
        "display": "💫 Gold Nouveau",
        "value": "gold-nouveau"
    },
    {
        "display": "⭕ Crimson",
        "value": "crimson"
    }
]

export const RoyalSlate: CustomThemeConfig = {
    name: 'royal-slate',
    properties: {
		// =~= Theme Properties =~=
		"--theme-font-family-base": `system-ui`,
		"--theme-font-family-heading": `ui-serif, Georgia, Cambria, 'Times New Roman', Times, serif`,
		"--theme-font-color-base": "0 0 0",
		"--theme-font-color-dark": "255 255 255",
		"--theme-rounded-base": "0px",
		"--theme-rounded-container": "0px",
		"--theme-border-base": "1px",
		// =~= Theme On-X Colors =~=
		"--on-primary": "255 255 255",
		"--on-secondary": "0 0 0",
		"--on-tertiary": "0 0 0",
		"--on-success": "0 0 0",
		"--on-warning": "0 0 0",
		"--on-error": "255 255 255",
		"--on-surface": "255 255 255",
		// =~= Theme Colors  =~=
		// primary | #2b136d 
		"--color-primary-50": "223 220 233", // #dfdce9
		"--color-primary-100": "213 208 226", // #d5d0e2
		"--color-primary-200": "202 196 219", // #cac4db
		"--color-primary-300": "170 161 197", // #aaa1c5
		"--color-primary-400": "107 90 153", // #6b5a99
		"--color-primary-500": "43 19 109", // #2b136d
		"--color-primary-600": "39 17 98", // #271162
		"--color-primary-700": "32 14 82", // #200e52
		"--color-primary-800": "26 11 65", // #1a0b41
		"--color-primary-900": "21 9 53", // #150935
		// secondary | #ffbe1d 
		"--color-secondary-50": "255 245 221", // #fff5dd
		"--color-secondary-100": "255 242 210", // #fff2d2
		"--color-secondary-200": "255 239 199", // #ffefc7
		"--color-secondary-300": "255 229 165", // #ffe5a5
		"--color-secondary-400": "255 210 97", // #ffd261
		"--color-secondary-500": "255 190 29", // #ffbe1d
		"--color-secondary-600": "230 171 26", // #e6ab1a
		"--color-secondary-700": "191 143 22", // #bf8f16
		"--color-secondary-800": "153 114 17", // #997211
		"--color-secondary-900": "125 93 14", // #7d5d0e
		// tertiary | #0EA5E9 
		"--color-tertiary-50": "219 242 252", // #dbf2fc
		"--color-tertiary-100": "207 237 251", // #cfedfb
		"--color-tertiary-200": "195 233 250", // #c3e9fa
		"--color-tertiary-300": "159 219 246", // #9fdbf6
		"--color-tertiary-400": "86 192 240", // #56c0f0
		"--color-tertiary-500": "14 165 233", // #0EA5E9
		"--color-tertiary-600": "13 149 210", // #0d95d2
		"--color-tertiary-700": "11 124 175", // #0b7caf
		"--color-tertiary-800": "8 99 140", // #08638c
		"--color-tertiary-900": "7 81 114", // #075172
		// success | #84cc16 
		"--color-success-50": "237 247 220", // #edf7dc
		"--color-success-100": "230 245 208", // #e6f5d0
		"--color-success-200": "224 242 197", // #e0f2c5
		"--color-success-300": "206 235 162", // #ceeba2
		"--color-success-400": "169 219 92", // #a9db5c
		"--color-success-500": "132 204 22", // #84cc16
		"--color-success-600": "119 184 20", // #77b814
		"--color-success-700": "99 153 17", // #639911
		"--color-success-800": "79 122 13", // #4f7a0d
		"--color-success-900": "65 100 11", // #41640b
		// warning | #EAB308 
		"--color-warning-50": "252 244 218", // #fcf4da
		"--color-warning-100": "251 240 206", // #fbf0ce
		"--color-warning-200": "250 236 193", // #faecc1
		"--color-warning-300": "247 225 156", // #f7e19c
		"--color-warning-400": "240 202 82", // #f0ca52
		"--color-warning-500": "234 179 8", // #EAB308
		"--color-warning-600": "211 161 7", // #d3a107
		"--color-warning-700": "176 134 6", // #b08606
		"--color-warning-800": "140 107 5", // #8c6b05
		"--color-warning-900": "115 88 4", // #735804
		// error | #D41976 
		"--color-error-50": "249 221 234", // #f9ddea
		"--color-error-100": "246 209 228", // #f6d1e4
		"--color-error-200": "244 198 221", // #f4c6dd
		"--color-error-300": "238 163 200", // #eea3c8
		"--color-error-400": "225 94 159", // #e15e9f
		"--color-error-500": "212 25 118", // #D41976
		"--color-error-600": "191 23 106", // #bf176a
		"--color-error-700": "159 19 89", // #9f1359
		"--color-error-800": "127 15 71", // #7f0f47
		"--color-error-900": "104 12 58", // #680c3a
		// surface | #5c637c 
		"--color-surface-50": "231 232 235", // #e7e8eb
		"--color-surface-100": "222 224 229", // #dee0e5
		"--color-surface-200": "214 216 222", // #d6d8de
		"--color-surface-300": "190 193 203", // #bec1cb
		"--color-surface-400": "141 146 163", // #8d92a3
		"--color-surface-500": "92 99 124", // #5c637c
		"--color-surface-600": "83 89 112", // #535970
		"--color-surface-700": "69 74 93", // #454a5d
		"--color-surface-800": "55 59 74", // #373b4a
		"--color-surface-900": "45 49 61", // #2d313d
		
	}
}