import { GitContentSource } from "@stackbit/cms-git";
import type { Model } from "@stackbit/types";
import { LirioModels } from "./models.js";
import { getLocalizedFieldForLocale } from "@stackbit/types";
import type { SiteMapEntry } from "@stackbit/types";

export function lirioConfig(dirname: string) {
    return {
        stackbitVersion: '~0.6.0',
        nodeVersion: '18',
        ssgName: 'custom',
        devCommand: './node_modules/.bin/vite --port {PORT}',
        experimental: {
            ssg: {
                name: 'sveltekit',
                logPatterns: {
                    up: [' ready in '],
                },
                passthrough: ['/vite-hmr/**'],
            },
        },
        presetSource: {
            type: "files",
            presetDirs: ["src/content/presets"]
        },
        contentSources: [
            new GitContentSource({
                rootPath: dirname,
                contentDirs: ["src/content"],
                models: getLirioModels(),
                assetsConfig: {
                    referenceType: "static",
                    assetsDir: "static",
                    staticDir: "static",
                    uploadDir: "uploads",
                    publicPath: "/",
                }
            })
        ],
        siteMap: ({documents, models}) => {
            const docs = documents.map(document => {
                if (["Page", "Collection"].includes(document.modelName)) {
                    const slug = getLocalizedFieldForLocale(document.fields.slug);
                    let collection = getLocalizedFieldForLocale(document.fields.collection);
                    if (collection) {
                        collection = collection.refId.slice(24, -5).toLowerCase()
                    }
                    const urlPath = '/' + (collection ? `${collection}/` : "") + slug.value.replace(/^\/+/, '').toLowerCase();
                    return {
                        stableId: document.id,
                        urlPath,
                        document,
                        isHomePage: urlPath === '/',
                    } satisfies SiteMapEntry
                }
                else if (document.modelName === "Homepage") {
                    return {document, urlPath: "/"}
                }
                return { document };
            }) as SiteMapEntry[];
            return docs;
        },
        sidebarButtons: getLirioButtons()
    }; 
}

// Stackbit models for Lirio components, fields, types, settings, and buttons
export function getLirioModels(): Model[] {
    return [
        ...pages,
        ...reusableFields,
        // TODO Use this method to create "presets", or collections of LirioBlocks that go well together.
        // TODO These will be selectively added to this array based on supplied user configuration when calling lirioConfig
        // TODO Also provide an override method that will add specific components via their Block reference ID.
        ...LirioModels,
        ...getLirioSettings()
    ]
}

// Sidebar buttons configurations for built-in Lirio elements in Stackbit
export function getLirioButtons() {
    return [
        {
            label: "Global Settings",
            type: "model",
            icon: "tools",
            modelName: "GlobalSettings"
        }
    ]
}

// Models for settings in Lirio sites
export function getLirioSettings() {
    return [
        {
            name: "GlobalSettings",
            description: "Global settings for the website",
            type: "data",
            filePath: "src/content/data/settings.json",
            fields: [
                {
                    name: "BaseURL",
                    type: "string",
                    default: "",
                    required: true,
                    description: "The URL of the final site. Used for resolving Metadata links."
                },
                {
                    name: "Menu",
                    type: "model",
                    models: ["MenuSettings"]
                },
                {
                    name: "Footer",
                    type: "model",
                    models: ["FooterSettings"]
                }
            ]
        } satisfies Model
    ];
}

// Page models used in Lirio sites
const pages = [
    {
        name: "Page",
        description: "Pages on the website",
        type: "page",
        filePath: "src/content/pages/{slug}.json",
        hideContent: true,
        fieldGroups: [
            {
                name: "meta",
                label: "Metadata",
                icon: "circle-info"
            }
        ],
        fields: [
            {
                name: "slug",
                type: "slug",
                required: true,
                group: "meta"
            },
            {
                name: "collection",
                type: "reference",
                required: false,
                models: ["Collection"]
            },
            {
                name: "title",
                type: "string",
                required: true,
                group: "meta"
            },
            {
                name: "description",
                type: "text",
                required: true,
                group: "meta"
            },
            {
                name: "MetaImage",
                type: "image",
                required: false,
                group: "meta"
            },
            {
                name: "blocks",
                type: "list",
                items: {
                    type: "model",
                    models: [],
                    groups: ["LirioBlock"]
                },
                default: []
            }
        ]
    },
    {
        name: "Collection",
        description: "Collections of pages on the website",
        type: "page",
        filePath: "src/content/collections/{slug}.json",
        hideContent: true,
        fieldGroups: [
            {
                name: "meta",
                label: "Metadata",
                icon: "circle-info"
            }
        ],
        fields: [
            {
                name: "slug",
                type: "slug",
                required: true,
                group: "meta"
            },
            {
                name: "title",
                type: "string",
                required: true,
                group: "meta"
            },
            {
                name: "description",
                type: "text",
                required: true,
                group: "meta"
            },
            {
                name: "MetaImage",
                type: "image",
                required: false,
                group: "meta"
            },
            {
                name: "blocks",
                type: "list",
                items: {
                    type: "model",
                    models: [],
                    groups: ["LirioBlock"]
                }
            }
        ]
    },
    {
        name: "Homepage",
        description: "Homepage of the Website",
        type: "page",
        filePath: "src/content/home.json",
        hideContent: true,
        fieldGroups: [
            {
                name: "meta",
                label: "Metadata",
                icon: "circle-info"
            }
        ],
        fields: [
            {
                name: "title",
                type: "string",
                default: "Homepage",
                required: true,
                group: "meta"
            },
            {
                name: "description",
                type: "text",
                required: true,
                group: "meta"
            },
            {
                name: "MetaImage",
                type: "image",
                required: false,
                group: "meta"
            },
            {
                name: "blocks",
                type: "list",
                items: {
                    type: "model",
                    models: [],
                    groups: ["LirioBlock"]
                }
            }
        ]
    },
] satisfies Model[]

// Fields re-used in various Lirio components
const reusableFields = [
    {
        name: "BackgroundImage",
        type: "object",
        fields: [
            {
                name: "Image",
                type: "image",
                default: "",
                required: true
            },
            {
                name: "AltText",
                type: "string",
                default: "",
                required: true
            },
            {
                name: "DarkeningPercentage",
                type: "enum",
                controlType: "dropdown",
                options: [
                    { label: "0%", value: 0 },
                    { label: "10%", value: 10 },
                    { label: "20%", value: 20 },
                    { label: "30%", value: 30 },
                    { label: "40%", value: 40 },
                    { label: "50%", value: 50 },
                    { label: "60%", value: 60 },
                    { label: "70%", value: 70 },
                    { label: "80%", value: 80 },
                    { label: "90%", value: 90 },
                    { label: "100%", value: 100 }
                ],
                default: 0
            },
            {
                name: 'Styles',
                type: 'style',
                styles: {
                    self: { 
                        width: '*'
                    }
                },
                default: {
                    self: { 
                        width: "full"
                    }
                }
            },
        ]
    },
    {
        name: "TeamCard",
        type: "object",
        fields: [
            {
                name: "BackgroundImage",
                type: "model",
                models: ['BackgroundImage'],
                required: true
            },
            {
                name: "Name",
                type: "string",
                default: "Member Name",
                required: true
            },
            {
                name: "Title",
                type: "string",
                default: "Team Member",
                required: true
            },
            {
                name: "Description",
                type: "markdown",
                default: "Anim aute id magna aliqua ad ad non deserunt sunt. Qui irure qui lorem cupidatat commodo. Elit sunt amet fugiat veniam occaecat fugiat aliqua.",
                required: true
            }
        ]
    },
    {
        name: "Icon",
        type: "object",
        fields: [
            {
                name: "Image",
                type: "image",
                default: "",
                required: true
            },
            {
                name: "AltText",
                type: "string",
                default: "",
                required: true
            },
            {
                name: "Url",
                type: "string",
                default: ""
            }
        ]
    },
    {
        name: "Link",
        type: "object",
        fields: [
            {
                name: "Label",
                type: "string",
                default: "Label",
                required: true
            },
            {
                name: "Url",
                type: "string",
                default: "#",
                required: true
            }
        ]
    },
    {
        name: "PopupMenu",
        type: "object",
        fields: [
            {
                name: "Label",
                type: "string",
                default: "Label",
                required: true
            },
            {
                name: "Event",
                type: "enum",
                controlType: "button-group",
                options: [
                    {label: "Click", value: "click"},
                    {label: "Hover", value: "hover"}
                ],
                required: true
            },
            {
                name: "Placement",
                type: "enum",
                controlType: "button-group",
                options: [
                    {label: "Bottom", value: "bottom"},
                    {label: "Top", value: "top"},
                    {label: "Left", value: "left"},
                    {label: "Right", value: "right"}
                ],
                required: true
            },
            {
                name: "Links",
                type: "list",
                items: {
                    type: "model",
                    models: ["Link"]
                }
            },
        ]
    },
    {
        name: "Button",
        type: "object",
        fields: [
            {
                name: "Label",
                type: "string",
                default: "Label",
                required: true
            },
            {
                name: "Url",
                type: "string",
                default: "#",
                required: true
            },
            {
                name: "Arrow",
                type: "boolean",
                default: false,
                required: true
            },
            {
                name: "Variant",
                type: "model",
                models: ["Variant"]
            },
            {
                name: "ArrowVariant",
                type: "model",
                models: ["Variant"]
            }
        ]
    },
    {
        name: "Variant",
        type: "object",
        fields: [
            {
                name: "value",
                type: "enum",
                options: [
                    {label: "Surface - Filled", value: "variant-filled-surface"},
                    {label: "Surface - Glass", value: "variant-glass-surface"},
                    {label: "Surface - Soft", value: "variant-soft-surface"},
                    {label: "Primary - Filled", value: "variant-filled-primary"},
                    {label: "Primary - Glass", value: "variant-glass-primary"}
                    {label: "Primary - Soft", value: "variant-soft-surface"},
                ]
            }
        ]
    },
    {
        name: "Image",
        type: "object",
        fields: [
            {
                name: "Image",
                type: "image",
                default: "",
                required: true
            },
            {
                name: "AltText",
                type: "string",
                default: "",
                required: true
            }
        ]
    },
] satisfies Model[]