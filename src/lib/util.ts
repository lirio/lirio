import {clsx} from 'clsx';

export function getContainerWidthClass(width: "narrow" | "wide" | "full"): string {
    return clsx(
        width === "narrow" && "max-w-3xl", 
        width === "wide" && "max-w-6xl", 
        width === "full" && "max-w-none"
    )
}

export function getVariantClass(variant: {label: string, value: string}): string {
    return clsx(
        variant.value === "variant-filled-surface" && "variant-filled-surface",
        variant.value === "variant-glass-surface" && "variant-glass-surface",
        variant.value === "variant-soft-surface" && "variant-soft-surface",
        variant.value === "variant-filled-primary" && "variant-filled-primary",
        variant.value === "variant-glass-primary" && "variant-glass-primary",
        variant.value === "variant-soft-primary" && "variant-soft-primary"
    )
}